export class Easing {
    public static easeInExpo(x: number) {
        return x === 0 ? 0 : Math.pow(2, 10 * x - 10);
    }
}