export default interface Word {
    text: string,
    style?: string
}